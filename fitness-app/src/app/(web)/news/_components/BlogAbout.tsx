import Image from "next/image";
import Link from "next/link";

const BlogAbout = () => {
  return (
    <div className="py-[100px]">
        <div className='wrapper'>
            <div className='w-[90%] mx-auto'>
                <h3 className="text-[#524FF5] text-[20px] Inter-Regular tracking-widest">BLOG</h3>
                <h2 className="text-[#1C2129] text-[33px] Inter-Bold mb-[20px]">Articles & News</h2>
                <ul className="flex flex-wrap">
                    <li className="box flex justify-between flex-wrap py-[30px] px-[30px] w-[31%] border-x border-y border-solid border-[#D2D2D2] cursor-pointer">
                        <div className="w-[100%] mb-[20px]">
                            <Image 
                                src={require("../../../../../public/assets/images/fitness-1.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="flex mb-[20px]">
                            <h3 className="text-[16px] text-[#524FF5] Inter-Regular mr-[10px]">March 22, 2022</h3>
                            <div className="px-[5px] bg-[#F3F3F3] mr-[30px]"><span className="Inter-Medium">Fitness</span></div>
                            <div className="px-[5px] bg-[#F3F3F3]"><span className="Inter-Medium">Health</span></div>
                        </div>
                        <h4 className="mb-[20px] text-[23px] text-[#1C2229] Inter-Bold">The 10 best exercises to do in your park</h4>
                        <div className="flex items-center">
                            <div className="w-[15%] mr-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/Jacob.png")} 
                                    className="rounded-full"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <span className="Inter-Medium text-[16px]">Jacob Cornish</span>
                        </div>
                    </li>
                    <li className="box flex justify-between flex-wrap py-[30px] px-[30px] w-[31%] border-x border-y border-solid border-[#D2D2D2] cursor-pointer"><Link href="news/blog-single">
                        <div className="w-[100%] mb-[20px]">
                            <Image 
                                src={require("../../../../../public/assets/images/fitness-2.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="flex mb-[20px]">
                            <h3 className="text-[16px] text-[#524FF5] Inter-Regular mr-[10px]">March 22, 2022</h3>
                            <div className="px-[5px] bg-[#F3F3F3] mr-[30px]"><span className="Inter-Regular">Fitness</span></div>
                            <div className="px-[5px] bg-[#F3F3F3]"><span className="Inter-Medium">Health</span></div>
                        </div>
                        <h4 className="mb-[20px] Inter-Bold text-[23px] text-[#1C2229]">How to Choose The Right Equipment For You</h4>
                        <div className="flex items-center">
                            <div className="w-[15%] mr-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/gray.svg")} 
                                    className="rounded-full"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <span className="Inter-Regular text-[16px] Inter-Medium">Benjamin Gray</span>
                        </div>
                        </Link>
                    </li>
                    <li className="box flex justify-between flex-wrap py-[30px] px-[30px] w-[31%] border-x border-y border-solid border-[#D2D2D2] cursor-pointer">
                        <div className="w-[100%] mb-[20px]">
                            <Image 
                                src={require("../../../../../public/assets/images/fitness-3.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="flex mb-[20px]">
                            <h3 className="text-[16px] text-[#524FF5] Inter-Regular mr-[10px]">March 22, 2022</h3>
                            <div className="px-[5px] bg-[#F3F3F3] mr-[30px]"><span className="Inter-Medium">Fitness</span></div>
                            <div className="px-[5px] bg-[#F3F3F3]"><span className="Inter-Medium">Health</span></div>
                        </div>
                        <h4 className="mb-[20px] text-[23px] text-[#1C2229] Inter-Bold">The 10 best exercises to do in your park</h4>
                        <div className="flex items-center">
                            <div className="w-[15%] mr-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/david.svg")} 
                                    className="rounded-full"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <span className="text-[16px] Inter-Medium">Jacob Cornish</span>
                        </div>
                    </li>
                    <li className="box flex justify-between flex-wrap py-[30px] px-[30px] w-[31%] border-x border-y border-solid border-[#D2D2D2] cursor-pointer">
                        <div className="w-[100%] mb-[20px]">
                            <Image 
                                src={require("../../../../../public/assets/images/fitness-4.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="flex mb-[20px]">
                            <h3 className="text-[16px] text-[#524FF5] Inter-Regular mr-[10px]">March 22, 2022</h3>
                            <div className="px-[5px] bg-[#F3F3F3] mr-[30px]"><span className="Inter-Medium">Fitness</span></div>
                            <div className="px-[5px] bg-[#F3F3F3]"><span className="Inter-Medium">Health</span></div>
                        </div>
                        <h4 className="mb-[20px] text-[23px] text-[#1C2229] Inter-Bold">Simple Condition for all Around Fitness.</h4>
                        <div className="flex items-center">
                            <div className="w-[15%] mr-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/william.svg")} 
                                    className="rounded-full"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <span className="text-[16px] Inter-Medium">William Wilkins</span>
                        </div>
                    </li>
                    <li className="box flex justify-between flex-wrap py-[30px] px-[30px] w-[31%] border-x border-y border-solid border-[#D2D2D2] cursor-pointer">
                        <div className="w-[100%] mb-[20px]">
                            <Image 
                                src={require("../../../../../public/assets/images/fitness-5.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="flex mb-[20px]">
                            <h3 className="text-[16px] text-[#524FF5] Inter-Regular mr-[10px]">March 22, 2022</h3>
                            <div className="px-[5px] bg-[#F3F3F3] mr-[30px]"><span className="Inter-Medium">Fitness</span></div>
                            <div className="px-[5px] bg-[#F3F3F3]"><span className="Inter-Medium">Health</span></div>
                        </div>
                        <h4 className="mb-[20px] text-[23px] text-[#1C2229] Inter-Bold">How to Modify any Program to Improve Your Weakness</h4>
                        <div className="flex items-center">
                            <div className="w-[15%] mr-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/foyid.svg")} 
                                    className="rounded-full"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <span className="text-[16px] Inter-Medium">Floyid Miles</span>
                        </div>
                    </li>
                    <li className="box flex justify-between flex-wrap py-[30px] px-[30px] w-[31%] border-x border-y border-solid border-[#D2D2D2] cursor-pointer">
                        <div className="w-[100%] mb-[20px]">
                            <Image 
                                src={require("../../../../../public/assets/images/fitness-6.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="flex mb-[20px]">
                            <h3 className="text-[16px] text-[#524FF5] Inter-Regular mr-[10px]">March 22, 2022</h3>
                            <div className="px-[5px] bg-[#F3F3F3] mr-[30px]"><span className="Inter-Medium">Fitness</span></div>
                            <div className="px-[5px] bg-[#F3F3F3]"><span className="Inter-Medium">Health</span></div>
                        </div>
                        <h4 className="mb-[20px] text-[23px] text-[#1C2229] Inter-Bold">The Beginner’s Guide to Weight Lifting</h4>
                        <div className="flex items-center">
                            <div className="w-[15%] mr-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/bernadette.svg")} 
                                    className="rounded-full"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <span className="text-[16px] Inter-Medium">Bernadette</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div> 
  )
}

export default BlogAbout
