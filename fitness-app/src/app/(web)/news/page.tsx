import BlogSpotlight from "./_components/BlogSpotlight"
import BlogAbout from "./_components/BlogAbout"

const page = () => {
  return (
    <div>
      <BlogSpotlight />
      <BlogAbout />
    </div>
  )
}

export default page
