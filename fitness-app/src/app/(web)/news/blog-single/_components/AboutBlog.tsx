import Image from "next/image";
import Link from "next/link";

const AboutBlog = () => {
  return (
    <div className="py-[100px]">
        <div className='wrapper'>
            <div className="w-[90%] mx-auto">
                <h3 className="text-[16px] text-[#524FF5] Inter-Medium mr-[10px]">March 22, 2022</h3>
                <h4 className="mb-[20px] text-[35px] text-[#1C2229] Inter-Bold w-[49%]">How to Modify any Program to Improve Your Weakness</h4>
                <div className="flex justify-between w-[100%] items-center mb-10">
                    <div className="px-[15px] py-[5px] bg-[#F3F3F3]"><span className="Inter-Medium">Fitness</span></div>
                    <div className="flex items-center w-[14%]">
                        <div className="w-[23%] mr-[15px]">
                            <Image 
                                src={require("../../../../../../public/assets/images/gray.svg")} 
                                className="rounded-full"
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <span className="text-[16px] Inter-Medium">Benjamin Gray</span>
                    </div>
                </div>
                <div className="w-[100%] mb-10">
                    <Image 
                        src={require("../../../../../../public/assets/images/exercises.png")} 
                        className="w-[100%] h-[]"
                        width={1000}
                        height={1000}
                        alt="image"
                    />
                </div>
                <div className="w-[75%] mx-auto">
                    <p className=" text-[#6A6A6A]  text-[16px] Inter-Regular w-[90%] mb-5">Dynamically target high pay of intellectual capital customized technologies objectively integrateemerging core competencies before process centric communities dramatically evisculate holistic innovation rather Progressively maintained extensived infomediaries via extensible dramatically disseminates standardized metrics after objectively pursue diverse catalysts for change for interoperable meta-services.</p>
                    <h4 className="text-[35px] Inter-Bold">The 10 best exercises to do in your park</h4>
                    <p className="text-[#6A6A6A] text-[16px] Inter-Regular w-[90%] mb-5">Dynamically target high pay of intellectual capital customized technologies objectively integrateemerging core competencies before process centric communities dramatically evisculate holistic innovation rather Progressively maintained extensived infomediaries via extensible dramatically disseminates standardized metrics after objectively pursue diverse catalysts for change for interoperable meta-services.</p>
                    <div className="mb-10">
                        <h3 className="text-[17px] text-[#6A6A6A] Lato-Regular">• Dynamically target high-payoff intellectual capital for customized</h3>
                        <h3 className="my-5 text-[17px] text-[#6A6A6A] Lato-Regular">• Interactively procrastinate high-payoff content</h3>
                        <h3 className="text-[17px] text-[#6A6A6A] Lato-Regular">• Credibly reinter mediate backend ideas for cross-platform models</h3>
                    </div>
                </div>
                <div className="bg-[#524FF5] py-20 px-20 w-[85%] mx-auto my-10">
                    <div className="bg-[#A1F65E] py-20 px-20">
                       <h3 className="text-[23px] text-center  italic Inter-Medium">When an unknown printegalley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.</h3>
                    </div>
                </div>
                <div className="w-[75%] mx-auto">
                  <h4 className="text-[30px] Inter-Bold">The 3 steps for morning routines</h4>
                    <p className="text-[#6A6A6A] text-[16px] Inter-Regular w-[91%] mb-5">Quickly aggregates users and the worldwides potentialities Progressively plagiarized a resourced leveling commerce through resource leveling core competencies ramatically mesh low-risk high-yield alignments before transparent e-tailers.</p>
                    <div className="mb-10">
                        <h3 className="text-[17px] text-[#6A6A6A] Lato-Regular">1. It brings the right people together with all the right information and tools to get work done</h3>
                        <h3 className="my-5 text-[17px] text-[#6A6A6A] Lato-Regular">2. We provide operational efficiency, data security, and flexible scale</h3>
                        <h3 className="text-[17px] text-[#6A6A6A] Lato-Regular">3. Your best work, together in one package that works seamlessly from your computer</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default AboutBlog
