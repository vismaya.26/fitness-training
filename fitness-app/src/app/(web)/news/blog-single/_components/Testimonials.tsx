import Image from "next/image";


const Testimonials = () => {
  return (
    <div className="bg-[#F9F9F9] py-[100px]">
        <div className='wrapper'>
            <div  className="mx-auto">
                <div className="w-[10%] mx-auto mb-5">
                    <Image 
                        src={require("../../../../../../public/assets/images/benjamin.jpg")} 
                        className="rounded-full"
                        width={1000}
                        height={1000}
                        alt="image"
                    />
                </div>
                <h3 className="text-center text-[25px] Inter-SemiBold">Benjamin Gray</h3>
                <h3 className="text-center text-[16px] Inter-Regular">Professional Trainer</h3>
                <p className="w-[50%] mx-auto text-center text-[20px] Inter-Medium text-[#1C2129] italic my-5">When an unknown printegalley of type and scrambled it to 
                    make a type specimen book. It has but also leap into electronic typesetting.
                </p>
                <div className="flex justify-between items-center w-[10%] mx-auto">
                    <div className="w-[20%]">
                        <Image 
                            src={require("../../../../../../public/assets/images/Insta.svg")} 
                            className=""
                            width={1000}
                            height={1000}
                            alt="image"
                        />
                    </div>
                    <div className="w-[20%]">
                        <Image 
                            src={require("../../../../../../public/assets/images/Fb.svg")} 
                            className=""
                            width={1000}
                            height={1000}
                            alt="image"
                        />
                    </div>
                    <div className="w-[20%]">
                        <Image 
                            src={require("../../../../../../public/assets/images/Twiter.svg")} 
                            className=""
                            width={1000}
                            height={1000}
                            alt="image"
                        />
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Testimonials
