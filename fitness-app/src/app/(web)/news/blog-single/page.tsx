import AboutBlog from "./_components/AboutBlog";
import NewsBlog from "./_components/NewsBlog";
import Testimonials from "./_components/Testimonials";

import React from 'react'

const page = () => {
  return (
    <div>
        <AboutBlog />
        <Testimonials />
        <NewsBlog />
    </div>
  )
}

export default page
