import AboutSpotlight from "./_components/AboutSpotlight"
import Mission from "./_components/Mission"
import Trainers from "../_components/Trainers"
import Experience from "./_components/Experience"

const page = () => {
  return (
    <div>
      <AboutSpotlight />
      <Mission />
      <Experience />
       <Trainers />
    </div>
  )
}

export default page

