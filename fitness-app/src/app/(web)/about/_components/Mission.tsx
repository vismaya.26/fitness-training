import Image from "next/image";

const Mission = () => {
  return (
    <div className="py-[100px]">
        <div className="wrapper">
            <div className="flex w-[90%] mx-auto justify-between">
                <div className="left w-[48%]">
                    <h3 className='text-[#524FF5] text-[20px] Inter-Regular tracking-widest'>WELCOME</h3>
                    <h2 className="text-[#1C2129] text-[45px] Inter-Bold w-[65%] mb-[20px] leading-[50px]">The Story Behind Our Gym</h2>
                    <p className='text-[20px] text-[#6A6A6A] Lato-Regular w-[88%] mb-[30px]'>It is a long established fact that a reader will be distracted  by the readable content of a page when looking at its layout. point of using Lorem Ipsum is</p>
                    <div className="bg-[#F0F0F0] border-1 border-solid border-[#D2D2D2] py-[40px] px-[40px]">
                         <h2 className="[#1C2129] text-[30px] Inter-Bold w-[82%] mb-[10px]">Story</h2>
                         <p className="text-[18px] text-[#6A6A6A] Lato-Regular w-[97%] mb-[30px]">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.point of using Lorem Ipsum.</p>
                         <div className="w-[100%]">
                            <Image 
                                src={require("../../../../../public/assets/images/story.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                    </div>
                </div>
                <div className="right w-[48%]">
                    <div className="mission py-[44px] px-[44px] mb-20">
                        <div className="pt-40">
                           <h2 className="text-[#FFF] text-[25px] Inter-SemiBold">Our Mission</h2>
                           <p className="text-[#FFF] text-[18px] Lato-Regular w-[89%]">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.point of using Lorem Ipsum.</p>
                        </div>
                    </div>
                    <div className="bg-[#A1F65E] py-[60px] px-[40px]">
                        <div className="">
                           <h2 className="text-[#222] text-[25px] Inter-SemiBold">Our Value</h2>
                           <p className="text-[#1B2129] text-[18px] Lato-Regular  w-[89%]">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.point of using Lorem Ipsum.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
  )
}

export default Mission
