import Image from "next/image";
import Link from "next/link";


const Spotlight = () => {
  return (
    <div className="pt-[100px]">
       <div className="wrapper">
           <div className="about py-[150px]">
              <h1 className="text-[70px] text-[#fff] mx-auto w-[66%] text-center mb-[20px] Inter-Bold ">Start with us the body and mind clensing</h1>
           </div>
            <div className="w-[60%] mx-auto mt-[-11%]">
                <Image 
                    src={require("../../../../../public/assets/images/spotlight.png")} 
                    className=""
                    width={1000}
                    height={1000}
                    alt="image"
                />
            </div>
       </div>
    </div>
  )
}

export default Spotlight
