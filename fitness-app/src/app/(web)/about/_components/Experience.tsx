import Image from "next/image";
import Link from "next/link";


const Experience = () => {
  return (
    <div className="py-[100px]">
        <div className="w-[100%]">
            <Image 
                src={require("../../../../../public/assets/images/workout.png")} 
                className="w-[100%]"
                width={1000}
                height={1000}
                alt="image"
            />
        </div>
        <div className="top bg-[#1B2129] py-[100px]">
            <div className="wrapper">
                <div className="w-[90%] mx-auto">
                    <div className="flex justify-between items-center mb-[39px]">
                        <div className="">
                            <h3 className="text-[#A1F65E] text-[20px] Inter-Regular tracking-widest">VALUES</h3>
                            <h2 className="text-[#fff] text-[45px] Inter-Bold">My core work values</h2>
                        </div>
                        <Link href="" className="text-[18px] bg-[#A1F65E] border-1 border-solid border-[#BABABA] py-[20px] px-[35px] Inter-Medium">Book a Class</Link>
                    </div>
                    <ul className="flex justify-between">
                        <li className="w-[31%] py-[60px] px-[45px] border-1 border-solid border-[#BABABA] bg-[#2C323A]">
                            <div className="w-[25%] mb-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/Iconz.svg")} 
                                    className="w-[100%]"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <h2 className="text-[25px] Inter-Bold text-[#fff]">Certified trainer</h2>
                            <p className="text-[18px] text-[#BCBCBC] Lato-Regular w-[80%]">Bring to the table win survival strategies ensure proactive new domination.</p>
                        </li>
                        <li className="w-[31%] py-[60px] px-[45px] border-1 border-solid border-[#BABABA] bg-[#2C323A]">
                            <div className="w-[25%] mb-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/healthy-Eating.svg")} 
                                    className="w-[100%]"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <h2 className="text-[25px] Inter-Bold text-[#fff]">Nutrition & diet</h2>
                            <p className="text-[18px] text-[#BCBCBC] Lato-Regular w-[80%]">Bring to the table win survival strategies ensure proactive new domination.</p>
                        </li>
                        <li className="w-[31%] py-[60px] px-[45px] border-1 border-solid border-[#BABABA] bg-[#2C323A]">
                            <div className="w-[25%] mb-[20px]">
                                <Image 
                                    src={require("../../../../../public/assets/images/Personal-Trainer.svg")} 
                                    className="w-[100%]"
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <h2 className="text-[25px] Inter-Bold text-[#fff]">Years of experience</h2>
                            <p className="text-[18px] text-[#BCBCBC] Lato-Regular w-[80%]">Bring to the table win survival strategies ensure proactive new domination.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div className="wrapper bottom">
            <ul className="flex w-[90%] mx-auto">
                <li className="border-2 border-solid border-[#D2D2D2] py-[45px] px-[10px] w-[25%] text-center">
                    <h2 className="text-[#1B2129] text-[60px] Inter-Bold">10+</h2>
                    <h3 className="text-[18px] Lato-Bold">Year of Experience</h3>
                </li>
                <li className="border-2 border-solid border-[#D2D2D2] py-[45px] px-[10px]  w-[25%] text-center">
                    <h2 className="text-[#1B2129] text-[60px] Inter-Bold">500+</h2>
                    <h3 className="text-[18px] Lato-Bold">Happy Clients</h3>
                </li>
                <li className="border-2 border-solid border-[#D2D2D2] py-[45px] px-[10px]  w-[25%] text-center">
                    <h2 className="text-[#1B2129] text-[60px] Inter-Bold">50+</h2>
                    <h3 className="text-[18px] Lato-Bold">Expert Trainers</h3>
                </li>
                <li className="border-2 border-solid border-[#D2D2D2] py-[45px] px-[10px]  w-[25%] text-center">
                    <h2 className="text-[#1B2129] text-[60px] Inter-Bold">15k</h2>
                    <h3 className="text-[18px] Lato-Bold">Instagram followers</h3>
                </li>
            </ul>
        </div>
    </div>
  )
}

export default Experience
