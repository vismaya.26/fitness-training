import { StaticImageData } from "next/image"
import item1 from "../../../../../public/assets/images/Trainer-1.png";
import item2 from "../../../../../public/assets/images/Trainer-2.png";
import item3 from "../../../../../public/assets/images/Trainer-3.png";
import item4 from "../../../../../public/assets/images/Trainer-4.png";
import item5 from "../../../../../public/assets/images/Trainer-5.png";
import item6 from "../../../../../public/assets/images/Trainer-6.png";
import img1 from "../../../../../public/assets/images/Star-1.svg";
import Insta from "../../../../../public/assets/images/instagram 1.svg";
import fb from "../../../../../public/assets/images/Fb.last.svg";
import Twiter from "../../../../../public/assets/images/Twiter.last.svg";


export type data = {
    Id: number,
    src: StaticImageData,
    name: string,
    discription: string,

}

export const data =[
    {
        id: 1,
        src: item1,
        name: "Miranda",
        img: img1,
        subname: "Specialisations :",
        discription: "Crossfit Expoort, Nutrition & Rehab",
        icon1: Insta,
        icon2: fb,
    },
    {
        id: 2,
        src: item2,
        name: "Madison Froning",
        img: img1,
        subname: "Specialisations :",
        discription: "Crossfit Expoort, Nutrition & Rehab",
        icon1: Insta,
        icon2: fb,
        icon3: Twiter,
    },
    { 
        id: 3,
        src: item3,
        name: "Joshua Frankilin",
        img: img1,
        subname: "Specialisations :",
        discription: "Crossfit Expoort, Nutrition & Rehab",
        icon1: Insta,
        icon2: fb,
        icon3: item1,
    },
    {
        id: 4,
        src: item4,
        name: "Joshua Frankilin",
        img: img1,
        subname: "Specialisations :",
        discription: "Crossfit Expoort, Nutrition & Rehab",
        icon1: Insta,
        icon2: fb,
        icon3: item1,
    },
    {
        id: 5,
        src: item5,
        name: "Joshua Frankilin",
        img: img1,
        subname: "Specialisations :",
        discription: "Crossfit Expoort, Nutrition & Rehab",
        icon1: Insta,
        icon2: fb,
        icon3: item1,
    },
    {
        id: 6,
        src: item6,
        name: "Joshua Frankilin",
        img: img1,
        subname: "Specialisations :",
        discription: "Crossfit Expoort, Nutrition & Rehab",
        icon1: Insta,
        icon2: fb,
        icon3: item1,
    }
]