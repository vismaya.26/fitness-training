import TrainersSpotlight from "./_components/TrainersSpotlight"
import OurTrainers from "./_components/OurTrainers"
import Routines from "./_components/Routines"

const page = () => {
  return (
    <div>
        <TrainersSpotlight />
        <OurTrainers />
        <Routines />
    </div>
  )
}

export default page
