import Image from "next/image";
import {data} from "../../trainers/data/trainers"


const OurTrainers = () => {
  return (
    <div className="py-[100px]">
       <div className="wrapper">
        <div className="mx-auto w-[90%]">
            <div className="top">
                <h3 className="text-[20px] text-[#524FF5] Inter-Medium tracking-widest text-center">OUR TRAINERS</h3>
                <h2 className="text-center text-[45px] text-[#222] Inter-Bold mb-[30px]">We Trained You to Gain</h2>
            </div>
            <div className="bottom">
                <ul className="flex justify-between flex-wrap">
                {data.map((item,id)=>(
                    <li className="w-[31%] cursor-pointer mb-20">
                        <div className="w-[100%] mx-auto">
                            <Image 
                                src={item.src} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="py-[40px] px-[40px] bg-[#1B2129]">
                            <div className="flex justify-between">
                                <h3 className="text-[#fff] text-[18px] Inter-Regular">{item.name}</h3>
                                <div className="flex items-center justify-end w-[49%]">
                                    <div className="w-[12%]">
                                        <Image 
                                            src={item.img} 
                                            className=""
                                            width={1000}
                                            height={1000}
                                            alt="image"
                                        />
                                    </div>
                                    <span className="text-[16px] text-[#fff] Inter-Regular">/5</span>
                                </div>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] my-[22px]"></hr>
                            <h3 className="text-[18px] text-[#fff] Lato-Regular">{item.subname}</h3>
                            <h4 className="text-[18px] text-[#858585] Lato-Regular mb-[32px]">{item.discription}</h4>
                            <div className="flex justify-between w-[20%]">
                                <div className="w-[40%]">
                                    <Image 
                                        src={item.icon1} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <div className="w-[40%]">
                                    <Image 
                                        src={item.icon2} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                
                            </div>
                        </div>
                    </li>
                    ))}
                </ul>
            </div>  
        </div>  
       </div>
    </div>
  )
}

export default OurTrainers
