import React from 'react'

const TrainersSpotlight = () => {
  return (
    <div className="pt-[100px]">
        <div className='wrapper'>
            <div className="Trainers py-[150px]">
              <h1 className="text-[70px] text-[#fff] mx-auto w-[66%] text-center mb-[20px] Inter-Bold ">Meet our Trainers</h1>
            </div>
        </div>
    </div>
  )
}

export default TrainersSpotlight
