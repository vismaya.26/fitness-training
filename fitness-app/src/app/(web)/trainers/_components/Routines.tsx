import React from 'react'

const Routines = () => {
  return (
    <div className="">
        <div className='top text-center'>
          <h3 className='text-[#524FF5] text-[20px] Inter-Regular tracking-widest'>WELCOME</h3>
          <h2 className='text-[#1C2129] text-[45px] Inter-Bold mb-[20px] leading-[50px]'>Take a look at my routines</h2>
          <p className='text-[17px] text-[#6A6A6A] Lato-Regular w-[35%] mb-[30px] mx-auto'>It is a long established and fact that a reader will be distracted by the
              readabled content of a pages when looking at its layout.
          </p>
        </div>
    </div>
  )
}

export default Routines
