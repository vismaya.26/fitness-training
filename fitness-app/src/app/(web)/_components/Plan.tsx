import Image from "next/image";
import Link from "next/link";


const Plan = () => {
  return (
    <div className="py-[100px]">
       <div className="wrapper">
            <div className="w-[90%] mx-auto">
                <div className="top text-center">
                    <h3 className="text-[#524FF5] text-[18px] Inter-Regular tracking-widest">OUR PLAN</h3>
                    <h2 className="text-[#222] Inter-Bold text-[40px] mb-[43px]">Choose the Program</h2>
                </div>
                <div className="bottom">
                    <ul className="flex justify-between">
                        <li className="w-[31%] bg-[#F9F9F9] py-[54px] px-[34px]">
                            <div className="flex items-center justify-between w-[43%] mb-[40px]">
                                <div className="w-[20%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/group-1.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h2 className="text-[35px] Inter-Bold">Basic</h2>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                            <div className="text-center">
                                <h3 className="text-[30px] Inter-SemiBold">$25 / month</h3>
                                <h3 className="text-[18px] Inter-Medium">30% Off for Beginners</h3>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] mt-[50px]"></hr>
                            <div className="mt-[20px] text-center ">
                                <h3 className="text-[#524FF5] text-[20px] Inter-SemiBold mb-[20px]">1 Day Free Trial</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]">20 minutes of <span className="text-[#1B2129] Lato-Bold text-[18px]">Heart-pumping spin</span></h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]"> 20 minutes of strength training</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]"><span className="text-[#1B2129] Lato-Bold text-[18px]">50 Class</span> Times Available</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[40px]"> 20 minutes of invigorating yoga</h3>
                                <Link href="#" className=" text-[18px] border-2 border-solid border-[#222] bg-[#222] text-[#fff] py-[15px] px-[40px] w-[13%] Inter-Medium">Get Started</Link>
                            </div>
                           
                        </li>
                        <li className="w-[31%] bg-[#F9F9F9] py-[54px] px-[34px]">
                            <div className="flex items-center justify-between w-[65%] mb-[40px]">
                                <div className="w-[20%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/group-2.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h2 className="text-[35px] Inter-Bold ">Standard</h2>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                            <div className="text-center">
                                <h3 className="text-[30px] Inter-SemiBold mb-[10px]">$35 / month</h3>
                                <button className="mx-auto py-[9px] px-[21px] border-1 border-solid border-[#A1F65E] bg-[#A1F65E] Inter-Medium">Most popular</button>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] mt-[25px]"></hr>
                            <div className="mt-[20px] text-center ">
                                <h3 className="text-[#524FF5] text-[20px] Inter-SemiBold mb-[20px]">2 Week Free Trial</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]">30 minutes of <span className="text-[#1B2129] Lato-Bold text-[18px]">Heart-pumping spin</span></h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]"> 30 minutes of strength training</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]"><span className="text-[#1B2129] Lato-Bold text-[18px]">50 Class</span> Times Available</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]"> 20 minutes of invigorating yoga</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]">Unlimited CrossFit Classes</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]">one Full Month Free</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[40px]"> First 25 New Members Only</h3>
                                <Link href="#" className=" text-[18px] border-2 border-solid border-[#BABABA] text-[#222] py-[15px] px-[40px] w-[13%] Inter-Medium">Get Started</Link>
                            </div>
                        </li>
                        <li className="w-[31%] bg-[#F9F9F9] py-[54px] px-[34px]">
                            <div className="flex items-center justify-between w-[62%] mb-[40px]">
                                <div className="w-[17%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/icon-3.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h2 className="text-[35px] Inter-Bold">Premium</h2>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                           <div className="text-center">
                                <h3 className="text-[30px] Inter-SemiBold">$25 / month</h3>
                                <h3 className="text-[18px] Inter-Medium">30% Off for Beginners</h3>
                           </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] mt-[50px]"></hr>
                            <div className="mt-[20px] text-center ">
                                <h3 className="text-[#524FF5] text-[20px] Inter-SemiBold mb-[20px]">2 Week Free Trial</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]"> 50 minutes of<span className="text-[#1B2129] Lato-Bold text-[18px]">Heart-pumping spin</span></h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]"> 50 minutes of strength training</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[15px]"> <span className="text-[#1B2129] Lato-Bold text-[18px]">60 Class</span> Times Available</h3>
                                <h3 className="text-[18px] text-[#6A6A6A] Inter-Regular mb-[40px]"> 60 minutes of invigorating yoga</h3>
                                <Link href="#" className=" text-[18px] border-2 border-solid border-[#222] bg-[#222] text-[#fff] py-[15px] px-[40px] w-[13%] Inter-Medium">Get Started</Link>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
       </div>
    </div>
  )
}

export default Plan
