import Image from "next/image";

const Amenities = () => {
  return (
    <div className="py-[100px] bg-[#524FF5]">
          <div className="wrapper">
            <div className=" flex w-[90%] mx-auto ">
                <div className="left w-[50%]">
                    <div className="">
                        <h3 className="text-[#fff] Inter-Regular tracking-widest">GYM AMENITIES</h3>
                        <h2 className="Inter-Bold text-[40px] text-[#fff] mb-[15px]">The Unique Standard</h2>
                        <p className="text-[#fff] text-[16px] Inter-Regular w-[79%] mb-[40px]">Copy It is a long established fact that a reader will 
                            be distracted by the readable content of a page when looking 
                            at its layout. point of using Lorem Ipsum is
                        </p>
                    </div>
                    <div>
                        <div className="w-[80%]">
                            <Image 
                                src={require("../../../../public/assets/images/gym-trainer.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                    </div>
                </div>
                <div className="right w-[50%]">
                    <div className="">
                        <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                        <div className="">
                            <div className="bg-[#1B2129] py-[6px] px-[19px] w-[26%] flex justify-between items-center mb-[10px]">
                                <div className="w-[16%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/tik.png")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h3 className="Inter-Medium text-[18px] text-[#fff]">LOCKERS</h3>
                            </div>
                            <p className="text-[18px] text-[#fff] Lato-Regular">Secure lockers that work by you choosing a four digit code.</p>
                        </div>
                        <hr className="border-1 border-solid border-[#D2D2D2] mt-[25px]"></hr>
                    </div>
                    <div className="">
                        <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                        <div className="">
                            <div className="bg-[#1B2129] py-[6px] px-[19px] w-[40%] flex justify-between items-center mb-[10px]">
                                <div className="w-[10%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/tik.png")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h3 className="Inter-Medium text-[18px] text-[#fff]">CHANGING ROOMS</h3>
                            </div>
                            <p className="text-[18px] text-[#fff] Lato-Regular w-[71%]">Delicious, protein-packed gourmet shakes that come in chocolate, fruit, vanilla and more. Water and snacks too.</p>
                        </div>
                        <hr className="border-1 border-solid border-[#D2D2D2] mt-[25px]"></hr>
                    </div>
                    <div className="">
                        <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                        <div className="">
                            <div className="bg-[#1B2129] py-[6px] px-[19px] w-[26%] flex justify-between items-center mb-[10px]">
                                <div className="w-[16%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/tik.png")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h3 className="Inter-Medium text-[18px] text-[#fff]">FUEL BAR</h3>
                            </div>
                            <p className="text-[18px] text-[#fff] Lato-Regular w-[76%]">Mini towels for class and lovely big towels for showers after class. *Amenities may not be available at all studios</p>
                        </div>
                        <hr className="border-1 border-solid border-[#D2D2D2] mt-[25px]"></hr>
                    </div>
                    <div className="">
                        <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                        <div className="">
                            <div className="bg-[#1B2129] py-[6px] px-[19px] w-[52%] flex justify-between items-center mb-[10px]">
                                <div className="w-[8%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/tik.png")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h3 className="Inter-Medium text-[18px] text-[#fff]">COMPLIMENTARY TOWELS</h3>
                            </div>
                            <p className="text-[18px] text-[#fff] Lato-Regular">Secure lockers that work by you choosing a four digit code.</p>
                        </div>
                        <hr className="border-1 border-solid border-[#D2D2D2] mt-[25px]"></hr>
                    </div>
                    <div className="">
                        <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                        <div className="">
                            <div className="bg-[#1B2129] py-[6px] px-[19px] w-[41%] flex justify-between items-center mb-[10px]">
                                <div className="w-[10%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/tik.png")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h3 className="Inter-Medium text-[18px] text-[#fff]">WIFI & RESTROOMS</h3>
                            </div>
                            <p className="text-[18px] text-[#fff] Lato-Regular">Secure lockers that work by you choosing a four digit code.</p>
                        </div>
                        <hr className="border-1 border-solid border-[#D2D2D2] mt-[25px]"></hr>
                    </div>
                    <div className="">
                        <hr className="border-1 border-solid border-[#D2D2D2] mb-[25px]"></hr>
                        <div className="">
                            <div className="bg-[#1B2129] py-[6px] px-[19px] w-[54%] flex justify-between items-center mb-[10px]">
                                <div className="w-[7%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/tik.png")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <h3 className="Inter-Medium text-[18px] text-[#fff]">HOT SHOWERS & HAIR CARE</h3>
                            </div>
                            <p className="text-[18px] text-[#fff] Lato-Regular w-[76%]">what your body craves in the morning, you're not alone. The majority of people crank the handle all the way up.</p>
                        </div>
                        <hr className="border-1 border-solid border-[#D2D2D2] mt-[25px]"></hr>
                    </div>
                </div>
            </div>
          </div>
    </div>
  )
}

export default Amenities
