import React from 'react'

const Schedule = () => {
  return (
    <div className="py-[100px] bg-[#1D2229]">
        <div className="wrapper">
             <div className="top text-center">
                <span className="Inter-Regular tracking-widest text-[20px] text-[#A1F65E]">TIME TABLE</span>
                <h2 className='text-[45px] text-[#fff] Inter-Bold'>Working Schedule</h2>
             </div>
        </div>
    </div>
  )
}

export default Schedule
