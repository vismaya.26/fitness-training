import Image from "next/image";
import Link from "next/link";

const About = () => {

  return (
    <div className="">
      <div className="border-2 border-solid border-[#222] bg-[#1D2229]">
            <h3 className="text-[60px] Inter-SemiBold text-[#fff] tracking-widest text-center">FOCUS ON YOUR <span className="text-[#A1F65E] Inter-SemiBold">FITNESS</span>  NOT YOUR LOSS</h3>
        </div>
        <div className="wrapper py-[100px]">
            <div className=" top flex w-[90%] mx-auto items-center mb-20">
              <div className=" w-[50%]">
                <h3 className='text-[#524FF5] text-[19px] Inter-Regular tracking-widest'>ABOUT</h3>
                <h3 className='text-[#1C2129] text-[45px] Inter-Bold w-[82%] mb-[20px] leading-[50px]'>Respect Your Body, It’s the Only One You Get</h3>
                <p className="text-[20px] text-[#6A6A6A] Lato-Regular w-[85%] mb-[30px]">It is a long established fact that  reader will be distracted by the readable content of a page when looking at its layout. point of using Lorem Ipsum is</p>
                  <div className="p-[30px] bg-[#F0F0F0] flex mb-[20px]">
                       <div className="">
                            <h3 className="text-[30px] Inter-Bold mb-[13px]">Motivation</h3>
                            <p className="text-[18px] text-[#6A6A6A] Lato-Regular w-[75%]">It is a long established fact that A reader will be distracted</p>
                        </div>
                        <div className="w-[50%] mx-auto">
                          <Image 
                              src={require("../../../../public/assets/images/gym-2.jpg")} 
                              className=""
                              width={1000}
                              height={1000}
                              alt="image"
                          />
                        </div>
                  </div>
                  <div className="p-[30px] bg-[#F0F0F0] flex">
                        <div className="w-[50%] mx-auto">
                          <Image 
                              src={require("../../../../public/assets/images/gym-3.png")} 
                              className=""
                              width={1000}
                              height={1000}
                              alt="image"
                          />
                        </div>
                        <div className="ml-[27px]">
                                <h3 className="text-[30px] Inter-Bold mb-[13px]">Inspire</h3>
                                <p className="text-[18px] text-[#6A6A6A] Lato-Regular w-[75%]">will be distracted by readable content using Lorem Ipsum</p>
                          </div>
                  </div>
              </div>
              <div className=" w-[50%] relative">
                <div className="w-[85%] mx-auto">
                    <Image 
                        src={require("../../../../public/assets/images/gym.jpg")} 
                        className=""
                        width={1000}
                        height={1000}
                        alt="image"
                    />
                </div>
                <Link href="#" className=" absolute text-[18px] border-2 border-solid border-[#fff] bg-[#fff] text-[#222] py-[15px] px-[30px] w-[30%] Inter-Medium right-[45%] bottom-[2%]">Get Started</Link>
              </div>
            </div>  
            <div className="bottom w-[90%] mx-auto">
              <hr className="border-1 border-solid border-[#D2D2D2] mb-[35px]"></hr>
               <ul className="flex justify-between w-[100%] cursor-pointer">
                <li className="w-[20%]">
                    <div className="flex items-center bg-[#F2F2F2] py-[8px] px-[10px]">
                        <div className="bg-[#222] py-[10px] px-[25px] w-[10%]"><span className="ml-[-8px] Inter-SemiBold text-[18px] text-[#fff] ">01</span></div>
                        <h4 className="Inter-Medium text-[18px] ml-[18px]">Fitnes Taining</h4>
                    </div>
                </li>
                <li className="w-[20%]">
                    <div className="flex items-center bg-[#F2F2F2] py-[8px] px-[10px]">
                        <div className="bg-[#222] py-[10px] px-[25px] w-[10%]"><span className="ml-[-8px] Inter-SemiBold text-[18px] text-[#fff]">02</span></div>
                        <h4 className="Inter-Medium text-[18px] ml-[18px]">Regular Routine</h4>
                    </div>
                </li>
                <li className="w-[20%]">
                    <div className="flex items-center bg-[#F2F2F2] py-[8px] px-[10px]">
                        <div className="bg-[#222] py-[10px] px-[25px] w-[10%]"><span className="ml-[-8px] Inter-SemiBold text-[18px] text-[#fff]">03</span></div>
                        <h4 className="Inter-Medium text-[18px] ml-[18px]">Deit Maintenance</h4>
                    </div>
                </li>
                <li className="w-[20%]">
                    <div className=" flex">
                        <div className="w-[100%]">
                        <Image 
                            src={require("../../../../public/assets/images/google.svg")} 
                            className=""
                            width={1000}
                            height={1000}
                            alt="image"
                        />
                        </div>
                        <div className="">
                           <h4 className="Inter-Medium text-[18px]">Review on Google</h4>
                           <div className="w-[40%]">
                              <Image 
                                src={require("../../../../public/assets/images/Star.svg")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                           </div>
                        </div>
                    </div>
                </li>
               </ul>
               <hr className="border-1 border-solid border-[#D2D2D2] mt-[35px]"></hr>
            </div>
        </div>
    </div>
  )
}

export default About

