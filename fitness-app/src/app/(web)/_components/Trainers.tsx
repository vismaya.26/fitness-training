import Image from "next/image";


const Trainers = () => {
  return (
    <div className="py-[100px] w-[90%] mx-auto">
        <div className="wrapper">
            <div className="top">
                <h3 className="text-[20px] text-[#524FF5] Inter-Medium tracking-widest text-center">OUR TRAINERS</h3>
                <h2 className="text-center text-[45px] text-[#222] Inter-Bold mb-[30px]">We Trained You to Gain</h2>
            </div>
            <div className="bottom">
                <ul className="flex justify-between ">
                    <li className="w-[31%]">
                        <div className="w-[100%] mx-auto">
                            <Image 
                                src={require("../../../../public/assets/images/Trainer-1.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="py-[40px] px-[40px] bg-[#1B2129]">
                            <div className="flex justify-between">
                                <h3 className="text-[#fff] text-[18px] Inter-Regular">Amanda</h3>
                                <div className="flex items-center justify-end w-[49%]">
                                    <div className="w-[12%]">
                                        <Image 
                                            src={require("../../../../public/assets/images/Star-1.svg")} 
                                            className=""
                                            width={1000}
                                            height={1000}
                                            alt="image"
                                        />
                                    </div>
                                    <span className="text-[16px] text-[#fff] Inter-Regular">/5</span>
                                </div>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] my-[22px]"></hr>
                            <h3 className="text-[18px] text-[#fff] Lato-Regular">Specialisations:</h3>
                            <h4 className="text-[18px] text-[#858585] Lato-Regular mb-[32px]">Crossfit Expoort, Nutrition & Rehab</h4>
                            <div className="flex justify-between w-[20%]">
                                <div className="w-[40%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/Insta.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <div className="w-[40%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/Fb.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                            </div>
                        </div>
                    </li>
                    <li className="w-[31%]">
                        <div className="w-[100%] mx-auto">
                            <Image 
                                src={require("../../../../public/assets/images/Trainer-2.png")} 
                                className="bg-[lightgray]"
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="py-[40px] px-[40px] bg-[#1B2129]">
                            <div className="flex justify-between">
                                <h3 className="text-[#fff] text-[18px] Inter-Regular">Madison Froning</h3>
                                <div className="flex items-center justify-end w-[49%]">
                                    <div className="w-[12%]">
                                        <Image 
                                            src={require("../../../../public/assets/images/Star-1.svg")} 
                                            className=""
                                            width={1000}
                                            height={1000}
                                            alt="image"
                                        />
                                    </div>
                                    <span className="text-[16px] text-[#fff] Inter-Regular">/5</span>
                                </div>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] my-[22px]"></hr>
                            <h3 className="text-[18px] text-[#fff] Lato-Regular">Specialisations:</h3>
                            <h4 className="text-[18px] text-[#858585] Lato-Regular mb-[32px]">Crossfit Expoort, Nutrition & Rehab</h4>
                            <div className="flex justify-between w-[20%] items-center">
                                <div className="w-[40%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/Insta.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <div className="w-[40%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/Fb.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                {/* <div className="w-[35%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/Twiter.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div> */}
                            </div>
                        </div>
                       
                    </li>
                    <li className="w-[31%]">
                        <div className="w-[100%] mx-auto">
                            <Image 
                                src={require("../../../../public/assets/images/Trainer-3.png")} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <div className="py-[40px] px-[40px] bg-[#1B2129]">
                            <div className="flex justify-between">
                                <h3 className="text-[#fff] text-[18px] Inter-Regular">Joshua Frankilin</h3>
                                <div className="flex items-center justify-end w-[49%]">
                                    <div className="w-[12%]">
                                        <Image 
                                            src={require("../../../../public/assets/images/Star-1.svg")} 
                                            className=""
                                            width={1000}
                                            height={1000}
                                            alt="image"
                                        />
                                    </div>
                                    <span className="text-[16px] text-[#fff] Inter-Regular">/5</span>
                                </div>
                            </div>
                            <hr className="border-1 border-solid border-[#D2D2D2] my-[22px]"></hr>
                            <h3 className="text-[18px] text-[#fff] Lato-Regular">Specialisations:</h3>
                            <h4 className="text-[18px] text-[#858585] Lato-Regular mb-[32px]">Crossfit Expoort, Nutrition & Rehab</h4>
                            <div className="flex justify-between w-[20%]">
                                <div className="w-[40%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/Insta.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                                <div className="w-[40%]">
                                    <Image 
                                        src={require("../../../../public/assets/images/Fb.svg")} 
                                        className=""
                                        width={1000}
                                        height={1000}
                                        alt="image"
                                    />
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
  )
}

export default Trainers
