import { StaticImageData } from "next/image"
import item1 from "../../../../../public/assets/images/frame-1.png";
import item2 from "../../../../../public/assets/images/frame-2.png";
import item3 from "../../../../../public/assets/images/frame-3.png";


export type data = {
    Id: number,
    src: StaticImageData,
    name: string,
    discription: string,

}

export const data =[
    {
        id: 1,
        src: item1,
        name: "Pilates Training",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    },
    {
        id: 2,
        src: item2,
        name: "Aerobic Training",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    },
    {
        id: 3,
        src: item3,
        name: "CrossFit Workout",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    }
]