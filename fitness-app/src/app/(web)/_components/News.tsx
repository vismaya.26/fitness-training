import Link from "next/link";
import Image from "next/image";


const News = () => {
  return (
    <div className="py-[100px]">
       <div className="wrapper">
            <div className="w-[90%] mx-auto">
               <h3 className="Inter-Bold text-[40px] text-[#222] mb-[30px]">Articles & News</h3>
                <div className="mb-[30px]">
                    <div className="bg-[#F9F9F9] py-[66px] px-[70px] w-[70%] mx-auto mb-[20px]"> 
                        <h4 className="text-[20px] Inter-Regular text-[#524FF5] mb-[20px]">March 23, 2022<span className="ml-[20px] Inter-Regular text-[#222]">Fitness</span></h4>
                        <div className="my-[32px]">
                            <h2 className="Inter-Bold text-[30px] text-[#222] mb-[13px]">The 10 best exercises to do in your park</h2>
                            <p className="text-[17px] Inter-Regular text-[#6A6A6A]">There are many variations of passages of Lorem Ipsum available, but the majority have alterationLorem ipsum dolor sit amet, consectetur adipiscing elit. Habitasse .</p>
                        </div>
                        <Link href="#" className=" text-[18px] border-2 border-solid border-[#BABABA] text-[#222] py-[15px] px-[40px] w-[13%] Inter-Medium">Read More</Link>
                    </div>
                    <div className="bg-[#F9F9F9] py-[66px] px-[70px] w-[70%] mx-auto"> 
                        <h4 className="text-[20px] Inter-Regular text-[#524FF5] mb-[20px]">March 23, 2022<span className="ml-[20px] Inter-Regular text-[#222]">Health</span></h4>
                        <div className="my-[32px]">
                            <h2 className="Inter-Bold text-[30px] text-[#222] mb-[13px]">The 10 best exercises to do in your park</h2>
                            <p className="text-[17px] Inter-Regular text-[#6A6A6A]">There are many variations of passages of Lorem Ipsum available, but the majority have alterationLorem ipsum dolor sit amet, consectetur adipiscing elit. Habitasse .</p>
                        </div>
                        <Link href="#" className=" text-[18px] border-2 border-solid border-[#BABABA] text-[#222] py-[15px] px-[40px] w-[13%] Inter-Medium">Read More</Link>
                    </div>
               </div>
               <div className="black py-[66px] px-[70px] mx-auto">
                    <h4 className="text-[20px] Inter-Regular text-[#fff] mb-[20px]">March 23, 2022<span className="ml-[20px] Inter-Regular text-[#222] border-2 border-solid border-[#fff] px-[13px] bg-[#fff]">Nutrition</span></h4>
                    <div className="my-[32px]">
                        <h2 className="Inter-Bold text-[30px] text-[#fff] mb-[13px]">The 10 best exercises to do in your park</h2>
                        <p className="text-[17px] Inter-Regular text-[#fff]">There are many variations of passages of Lorem Ipsum available, but the majority have alterationLorem ipsum dolor sit amet, consectetur adipiscing elit. Habitasse .</p>
                    </div>
                    <Link href="#" className=" text-[18px] border-1 border-solid border-[#BABABA] text-[#222] py-[15px] px-[40px] w-[13%] Inter-Medium bg-[#A1F65E]">Read More</Link>
               </div>
            </div>
       </div>
    </div>
  )
}

export default News
