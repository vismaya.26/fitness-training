import React from 'react'

const Spotlight = () => {
  return (
    <div className="pt-[100px]">
      <div className="wrapper">
          <div className="main py-[150px]">
               <h1 className="text-[70px] text-[#fff] mx-auto w-[40%] text-center mb-[20px] Inter-Bold">Keep Your Body Fit & Strong</h1>
               <div className="flex justify-between w-[30%] mx-auto">
                  <button className="py-[10px] [px-10px] border-2 border-solid border-[#A1F65E] bg-[#A1F65E] w-[48%] text-[20px] Inter-Regular">Start Today</button>
                  <button className="py-[10px] [px-10px] border-2 border-solid border-[#fff] w-[48%] text-[20px] text-[#fff] Inter-Regular">About Me</button>

               </div>
          </div>
      </div>
    </div>
  )
}

export default Spotlight

