"use client"

import Link from "next/link";
import Image from "next/image";
import {data} from "../_components/data/data"
import { useState } from "react";
import Read from "@/app/auth/_components/Read";



const Classes = () => {
   const [showRead, setShowRead] = useState(false);
  return (
    <div className="py-[100px]">
       <div className="wrapper">
            <div className="top flex justify-between items-center w-[90%] mx-auto mb-[55px]">
                <div className="">
                    <h3 className="text-[#524FF5] text-[20px] Inter-Regular tracking-widest">OUR FITNESS TRAINING</h3>
                    <h3 className="text-[#1C2129] text-[33px] Inter-Bold">Upcoming Classes</h3>
                </div>
                <Link href="#" className=" text-[18px] border-2 border-solid border-[#222] bg-[#222] text-[#fff] py-[15px] px-[30px] w-[13%] Inter-Medium">More Class</Link>
            </div>
            <div className="bottom">
                <ul className="flex justify-between w-[90%] mx-auto">
                    {data.map((item,id)=>(
                        <li className="w-[31%]">
                         <div className="w-[100%]">
                             <Image 
                                 src={item.src}
                                 className=""
                                 width={1000}
                                 height={1000}
                                 alt="image"
                             /> 
                          </div>
                         <h3 className="text-[30px] text-[#222] Inter-Bold my-[10px]">{item.name}</h3>
                         <p className="text-[#6A6A6A] text-[18px] Inter-Regular w-[85%] mb-[10px]">{item.discription}</p>
                         <button className="text-[#524FF5] text-[18px] Inter-Regular border-1 border-solid" onClick={() =>setShowRead(true)}>{item.button}</button>
                        </li>
                    ))}
                </ul>
            </div>
            <Read isVisible={showRead} onClose={() => setShowRead(false)} />
       </div>
    </div>
  )
}

export default Classes
