import Spotlight from "@/app/(web)/_components/Spotlight"
import Classes from "./_components/Classes"
import About from "./_components/About"
import Schedule from "./_components/Schedule"
import Trainers from "./_components/Trainers"
import Amenities from "./_components/Amenities"
import Plan from "./_components/Plan"
import News from "./_components/News"


const page = () => {
  return (
    <main>
      <Spotlight />
      <Classes />
      <About />
      <Schedule />
      <Trainers />
      <Amenities />
      <Plan />
      <News />
    </main>
  )
}

export default page

