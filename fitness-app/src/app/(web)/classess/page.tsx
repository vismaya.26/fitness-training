import ClassSpotlight from "./_components/ClassSpotlight"
import ClassAbout from "./_components/ClassAbout"
import Club from "./_components/Club"



const page = () => {
  return (
    <div>
       <ClassSpotlight />
       <ClassAbout />
       <Club />
    </div>
  )
}

export default page
