import React from 'react'

const SingleSpotlight = () => {
  return (
    <div className="">
       <div className='wrapper'>
            <div className="class-single py-[150px]">
              <h1 className="text-[70px] text-[#fff] mx-auto w-[66%] text-center mb-[20px] Inter-Bold ">Yoga As Therapy</h1>
            </div>

       </div>
    </div>
  )
}

export default SingleSpotlight
