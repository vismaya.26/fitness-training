import Image from "next/image";
import Link from "next/link";


const Details = () => {
  return (
    <div className="py-[100px]">
         <div className="wrapper">
                <div className="w-[90%] mx-auto">
                    <div className="flex justify-between mb-20">
                        <div className="w-[64%]">
                            <div className="w-[100%] mb-[30px]">
                                <Image 
                                    src={require("../../../../../../public/assets/images/yoga.png")} 
                                    className=""
                                    width={1000}
                                    height={1000}
                                    alt="image"
                                />
                            </div>
                            <h4 className="text-[18px] text-[#524FF5] Lato-Regular">January 12, 2024<span className="py-[5px] px-[16px] bg-[#F3F3F3] text-[18px] text-[#222] ml-[20px] Lato-Regular">Fat Loss</span></h4>
                        </div>
                        <div className="w-[34%]">
                            <div className="bg-[#F9F9F9] border-1 border-solid border-[#F9F9F9] py-[40px] px-[50px]">
                                <h2 className="text-[30px] Inter-Bold">Class Details</h2>
                                <p className="Lato-Regular text-[20px] mb-[30px] w-[91%]">Objectively innovate empowered manufactured products</p>
                                <div className="mb-[40px]">
                                    <h3 className="text-[18px] Inter-Regular text-[#6A6A6A] ">Duration : <span className="text-[18px] Inter-SemiBold text-[#222]">45 MINUTES</span></h3>
                                    <h3 className="text-[18px] Inter-Regular text-[#6A6A6A] my-[20px]" >Intensity :  <span className="text-[18px] Inter-SemiBold text-[#222]"> High</span></h3>
                                    <h3 className="text-[18px] Inter-Regular text-[#6A6A6A] my-[20px]">Fitness Level : <span className="text-[18px] Inter-SemiBold text-[#222]">Advanced</span></h3>
                                    <h3 className="text-[18px] Inter-Regular text-[#6A6A6A] mb-[40px]">Schedule : <span className="text-[18px] Inter-SemiBold text-[#222]">Monday, Saturday</span></h3>
                                    <Link href="#" className="text-[18px] border-2 border-solid border-[#fff] bg-[#222] text-[#fff] py-[15px] px-[30px] Inter-Medium">Book a Class</Link>
                                </div>
                            </div>
                        </div>
                   </div>
                    <div className="mb-10">
                        <h2 className="text-[35px] Inter-Bold">The Healthy Life Style For All</h2>
                        <p className="text-[17px] Lato-Regular w-[67%] mb-7">Proactively envisioned multimedia based expertise crosses media growth strategies. Seamlessly 
                            visualize quality intelectual captal without superor collaboration idea sharing Holistically pontficate 
                            installed based portals after maintainabled products. Phosfluorescently engaged world wide methodologies 
                            with enabled Completely pursue scalable customer service through sustainable potentialities
                        </p>
                        <div>
                            <h3 className="text-[17px] text-[#6A6A6A] Lato-Regular">• A natural way of your health.</h3>
                            <h3 className="my-5 text-[17px] text-[#6A6A6A] Lato-Regular">• Train Yourself to Exercise.</h3>
                            <h3 className="text-[17px] text-[#6A6A6A] Lato-Regular">• Enhancing the personal healing.</h3>
                        </div>
                   </div>
                   <div className="mb-20">
                        <h2 className="text-[25px] Inter-Bold">Make real time a health improvements</h2>
                        <p className="text-[17px] Lato-Regular w-[67%] mb-7">Proactively envisioned multimedia based expertise crosses media growth strategies. Seamlessly 
                            visualize quality intelectual captal without superor collaboration idea sharing Holistically pontficate 
                            installed based portals after maintainabled products. Phosfluorescently engaged world wide methodologies 
                            with enabled Completely pursue scalable customer service through sustainable potentialities
                        </p>
                        <div>
                            <h3 className="text-[17px] text-[#6A6A6A] Lato-Regular">1. It brings the right people together with all the right information and tools to get work done</h3>
                            <h3 className="my-5 text-[17px] text-[#6A6A6A] Lato-Regular">2. We provide operational efficiency, data security, and flexible scale</h3>
                            <h3 className="text-[17px] text-[#6A6A6A] Lato-Regular">3. Your best work, together in one package that works seamlessly from your computer</h3>
                        </div>
                   </div>
                   <div className="">
                        <div className="w-[100%] mb-[30px]">
                            <Image 
                                src={require("../../../../../../public/assets/images/Video.png")} 
                                className="w-[100%]"
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                   </div>
              </div>
         </div>
    </div>
  )
}

export default Details