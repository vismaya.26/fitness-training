import SingleSpotlight from "./_components/SingleSpotlight"
import Details from "./_components/Details"

const page = () => {
  return (
    <div>
       <SingleSpotlight />
       <Details />
    </div>
  )
}

export default page
