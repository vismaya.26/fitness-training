import React from 'react'

const ClassSpotlight = () => {
  return (
    <div className="pt-[100px]">
       <div className='wrapper'>
            <div className="class py-[150px]">
              <h1 className="text-[70px] text-[#fff] mx-auto w-[66%] text-center mb-[20px] Inter-Bold ">Classes</h1>
            </div>
       </div>
    </div>
  )
}

export default ClassSpotlight
