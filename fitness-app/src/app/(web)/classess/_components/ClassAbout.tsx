import Image from "next/image";
import Link from "next/link";
import {data} from "../_components/data/data"


const ClassAbout = () => {
  return (
    <div className="py-[100px]">
        <div className="wrapper">
            <div className="text-center mb-[30px]">
                 <h3 className="text-[#524FF5] text-[20px] Inter-Regular tracking-widest">Our Trainers</h3>
                 <h2 className="text-[#1C2129] text-[33px] Inter-Bold">We Trained You to Gain</h2>
            </div>
            <ul className="flex justify-between mx-auto w-[90%] flex-wrap">
                {data.map((item, id) => (
                    <li key={id} className="w-[31%] mb-[25px]">
                        <div className="w-[100%]">
                            <Image 
                                src={ item.src} 
                                className=""
                                width={1000}
                                height={1000}
                                alt="image"
                            />
                        </div>
                        <h3 className="text-[30px] text-[#222] Inter-Bold my-[10px]">{item.name}</h3>
                        <p className="text-[#6A6A6A] text-[18px] Inter-Regular w-[85%] mb-[10px]">{item.discription}</p>
                        <Link href="classess/class-single" className="text-[#524FF5] text-[18px] Inter-Regular ">{item.button}</Link>
                    </li>
                ))}
            </ul>
        </div>
    </div>
  )
}

export default ClassAbout
