import { StaticImageData } from "next/image"
import item1 from "../../../../../../public/assets/images/frame-1.png";
import item2 from "../../../../../../public/assets/images/frame-2.png";
import item3 from "../../../../../../public/assets/images/frame-3.png";
import item4 from "../../../../../../public/assets/images/frame-4.png";
import item5 from "../../../../../../public/assets/images/frame-5.png";
import item6 from "../../../../../../public/assets/images/frame-6.png";

export type data = {
    Id: number,
    src: StaticImageData,
    name: string,
    discription: string,

}





export const data = [
    {
        id: 1,
        src: item1,
        name: "Pilates Training",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    },
    {
        id: 2,
        src: item2,
        name: "Aerobic",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    },
    {
        id: 3,
        src: item3,
        name: "CrossFit",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    },
    {
        id: 4,
        src: item4,
        name: "Yoga As Therapy",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    },
    {
        id: 5,
        src: item5,
        name: "Boxing",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    },
    {
        id: 6,
        src: item6,
        name: "Energy Dancei,",
        discription: "containing Lorem Ipsum passagesand more recently with",
        button: "Read More"
    }
]