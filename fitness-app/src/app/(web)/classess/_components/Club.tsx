import Image from "next/image";
import Link from "next/link";


const Club = () => {
  return (
    <div className="py-[100px]">
        <div className='club text-center py-[150px]'>
             <h2 className="text-[45px] Inter-Bold text-[#fff] mb-[15px]">Join Our Club</h2>
             <p className=" mx-auto text-[18px] Lato-Regular text-[#fff] w-[31%] mb-[40px]">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. point of using Lorem Ipsum is</p>
                <Link href="" className="text-[18px] bg-[#A1F65E] border-1 border-solid border-[#BABABA] py-[20px] px-[35px] Inter-Medium ">Book a Class</Link>
        </div>
    </div>
  )
}

export default Club
