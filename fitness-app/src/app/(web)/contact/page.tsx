import ContactSpotlight from "./_components/ContactSpotlight"
import AboutContact from "./_components/AboutContact"
import Info from "./_components/Info"
import Faq from "./_components/Faq"

const page = () => {
  return (
    <div>
      <ContactSpotlight  />
      <AboutContact />
      <Info />
      <Faq />
    </div>
  )
}

export default page
