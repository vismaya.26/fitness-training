import React from 'react'
import Link from "next/link";


const AboutContact = () => {
  return (
    <div className="py-[100px]">
          <div className="wrapper">
            <div className="flex w-[85%] mx-auto items-center">
                  <div className="left w-[50%]">
                    <h3 className="text-[#524FF5] text-[18px] Inter-Regular tracking-widest">WELCOME TO STRENGTHY</h3>
                    <h2 className="text-[#1C2129] text-[33px] Inter-Bold">Get In Touch With Us</h2>
                    <p className="Inter-Regular text-[16px] w-[75%] mb-5">If you have any feedback or questions about our clubs, our website or our services in General, please contact us by filling out the form.</p>
                    <h3 className="text-[#1C2129] text-[25px] Inter-Bold">Open Hours</h3>
                    <hr className="border-1 border-solid border-[#D3D3D3] w-[40%] my-4" />
                    <div className="">
                        <h3 className='text-[17px] Inter-SemiBold'>Mon – Fri :<span className='text-[#6A6A6A] text-[17px] Inter-Regular'> 08.00 AM To 09.00 PM</span></h3>
                        <h3 className=' my-5 text-[17px] Inter-SemiBold'>Sat :<span className='text-[#6A6A6A] text-[17px] Inter-Regular'> 09.00 AM To 06.00 PM</span></h3>
                        <h3 className='text-[17px] Inter-SemiBold'>sunday :<span className='text-[#6A6A6A] text-[17px] Inter-Regular'> 09.00 AM To 02.00 PM</span></h3>
                    </div>
                  </div>
                  <div className="right w-[50%]">
                       <div className="bg-[#F9F9F9] py-20 px-20">
                            <h4 className="text-[#1C2129] text-[30px] Inter-Bold">Send Us a Message</h4>
                            <h3 className="text-[#524FF5] text-[18px] Inter-Regular">Your email address will not be published *</h3>
                            <div className="border border-solid border-[#D3D3D3] py-[10px] px-[20px] mt-[32px] mb-[30px]">
                               <input type="text" placeholder="your full name" className="text-[17px] text-[#6A6A6A] Lato-Regular"/>
                            </div>
                            <div className="border border-solid border-[#D3D3D3] py-[10px] px-[20px] mb-[30px]">
                               <input type="text" placeholder="e-mall address" className="text-[17px] text-[#6A6A6A] Lato-Regular"/>
                            </div>
                            <div className="border border-solid border-[#D3D3D3] py-[10px] px-[20px] mb-[30px]">
                               <input type="text" placeholder="subject" className="text-[17px] text-[#6A6A6A] Lato-Regular"/>
                            </div>
                            <div className="border border-solid border-[#D3D3D3] py-[10px] px-[20px] mb-[30px]">
                               <textarea placeholder="message" className="text-[17px] text-[#423f3f] Lato-Regular h-[130px] w-full row-5 col-5"/>
                            </div>
                            <Link href="#" className=" text-[18px] border-2 border-solid border-[#222] bg-[#222] text-[#fff] py-[15px] px-[40px] w-[13%] Inter-Medium">Send Now</Link>
                       </div>
                  </div>
            </div>
          </div>
    </div>
  )
}

export default AboutContact
