import Image from "next/image";


const Faq = () => {
  return (
    <div className="py-[80px]">
       <div className="wrapper">
           <div className="">
                <h3 className="text-[#524FF5] text-[20px] Inter-SemiBold tracking-widest text-center">FAQ</h3>
                <h4 className="text-[#1C2129] text-[40px] Inter-Bold mb-10 text-center">Frequently Asked Questions</h4>
                <div className="flex justify-between mx-auto w-[55%] items-center">
                   <h3 className="text-[#1B2129] text-[18px] Inter-SemiBold">What is the Fitnessfit schedule?</h3>
                    <div className="w-[3%]">
                        <Image 
                            src={require("../../../../../public/assets/images/down.svg")} 
                            className=""
                            width={1000}
                            height={1000}
                            alt="image"
                        />
                    </div>
                </div>
                <hr className="border-1 border-solid border-[#D3D3D3] w-[55%] mx-auto my-5" />
                <div className="flex justify-between mx-auto w-[55%] items-center">
                   <h3 className="text-[#1B2129] text-[18px] Inter-SemiBold">Do I need previous experience for your classes?</h3>
                    <div className="w-[3%]">
                        <Image 
                            src={require("../../../../../public/assets/images/up.svg")} 
                            className=""
                            width={1000}
                            height={1000}
                            alt="image"
                        />
                    </div>
                </div>
                <p className="text-[#6A6A6A] text-[16px] Inter-Regular mx-[19rem] my-5 w-[45%]">Podcastings operationals changed a managements insides of works flows established frame worked takings seamless keys performanced indicators offline to maximise the ball while performing</p>
                <hr className="border-1 border-solid border-[#D3D3D3] w-[55%] mx-auto my-5" />
                <div className="flex justify-between mx-auto w-[55%] items-center">
                   <h3 className="text-[#1B2129] text-[18px] Inter-SemiBold">Do you offer on-site classes, or just virtual classes?</h3>
                    <div className="w-[3%]">
                        <Image 
                            src={require("../../../../../public/assets/images/down.svg")} 
                            className=""
                            width={1000}
                            height={1000}
                            alt="image"
                        />
                    </div>
                </div>
                <hr className="border-1 border-solid border-[#D3D3D3] w-[55%] mx-auto my-5" />
                <div className="flex justify-between mx-auto w-[55%] items-center">
                   <h3 className="text-[#1B2129] text-[18px] Inter-SemiBold">Do you offer a trial class for any of your classes?</h3>
                    <div className="w-[3%]">
                        <Image 
                            src={require("../../../../../public/assets/images/down.svg")} 
                            className=""
                            width={1000}
                            height={1000}
                            alt="image"
                        />
                    </div>
                </div>
           </div>
       </div>
    </div>
  )
}

export default Faq
