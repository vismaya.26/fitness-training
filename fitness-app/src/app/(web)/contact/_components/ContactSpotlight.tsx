import React from 'react'

const ContactSpotlight = () => {
  return (
    <div className='pb-10 pt-[100px]'>
        <div className='wrapper'>
            <div className="Contact py-[150px]">
              <h1 className="text-[70px] text-[#fff] mx-auto w-[66%] text-center mb-[20px] Inter-Bold ">Contact Us</h1>
            </div>
        </div>
    </div>
  )
}

export default ContactSpotlight
