import Image from "next/image";
import Link from "next/link";

const Info = () => {
  return (
    <div className="py-[100px]">
         <div className=" Details">
             <div className=" py-40 px-40">
                <div className="flex justify-between bg-[#A1F65E] py-10 px-20 w-[100%]">
                  <div className="w-[31%] py-5">
                      <div className="w-[19%] rounded-full bg-[#222] p-[10px] mx-auto">
                        <Image 
                          src={require("../../../../../public/assets/images/Call.svg")} 
                          className=""
                          width={1000}
                          height={1000}
                          alt="image"
                        />
                      </div>
                    <h3 className="text-[20px] Inter-Bold text-center my-2">Phone</h3>
                    <hr className="border-2 border-solid border-[#222] w-12 mx-auto"></hr>
                    <p className="text-[17px] w-[95%] Inter-Regular text-[#1B2129] text-center my-2">Capitalized on hanging frut to identify with additional</p>
                    <h3 className="text-center text-[16px] text-[#1B2129] Inter-Medium">(+01) 123 456 7890</h3>
                  </div>
                   {/* <hr className="line transform rotate-90" /> */}
                  <div className="w-[31%] py-5">
                      <div className="w-[18%] rounded-full bg-[#222] p-[10px] mx-auto">
                        <Image 
                          src={require("../../../../../public/assets/images/Mail.svg")} 
                          className=""
                          width={1000}
                          height={1000}
                          alt="image"
                        />
                      </div>
                    <h3 className="text-[20px] Inter-Bold text-center my-2">Mail</h3>
                    <hr className="border-2 border-solid border-[#222] w-10 mx-auto"></hr>
                    <p className="text-[17px] w-[95%] Inter-Regular text-[#1B2129] text-center my-2">Capitalized on hanging frut to identify with additional</p>
                    <h3 className="text-center text-[16px] text-[#1B2129] Inter-Medium">fitnessfit@gmail.com</h3>
                  </div>
                  {/* <hr className="line transform rotate-90" /> */}
                  <div className="w-[31%] py-5">
                      <div className="w-[19%] rounded-full bg-[#222] p-[10px] mx-auto">
                        <Image 
                          src={require("../../../../../public/assets/images/location.svg")} 
                          className=""
                          width={1000}
                          height={1000}
                          alt="image"
                        />
                      </div>
                    <h3 className="text-[20px] Inter-Bold text-center my-2">Location</h3>
                    <hr className="border-2 border-solid border-[#222] w-10 mx-auto"></hr>
                    <p className="text-[17px] w-[95%] Inter-Regular text-[#1B2129] text-center my-2">Capitalized on hanging frut to identify with additional</p>
                    <h3 className="text-center text-[16px] text-[#1B2129] Inter-Medium">123 King Dr.Oswego, NY 13126</h3>
                  </div>
                </div>
             </div>
         </div>
    </div>
  )
}

export default Info