"use client"

import Link from "next/link";
import Image from "next/image";


export default function Read({isVisible, onClose} :any) {
    if(!isVisible) return null;
    return (
        <div className="fixed top-0 left-0 flex items-center justify-center w-full h-full bg-black bg-opacity-75">
            <div className="flex flex-col justify-center w-2/3 h-2/3 mx-auto bg-black p-20 relative">
                <div className="absolute top-0 right-0 p-4">
                     <Link href="#" className=" text-white Inter-Bold " onClick={() => onClose()}>X</Link>
                </div>
                <div className="w-[10%] mx-auto">
                    <Image 
                        src={require("../../../../public/assets/images/Insta.svg")} 
                        className=""
                        width={1000}
                        height={1000}
                        alt="image"
                    />
                </div>
                <h1 className='text-[#fff] text-3xl text-center Inter-Bold  italic'>I WILL PROVE MY SKILLS</h1>
                <p className="text-center Inter-Regular">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minima nihil necessitatibus provident, rem error debitis earum dicta vitae totam aut officia iste explicabo voluptas recusandae. Ducimus qui vitae pariatur perspiciatis?</p>
            </div>
        </div>
    )
}
