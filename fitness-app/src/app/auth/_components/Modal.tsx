"use client"

import React from 'react'
import Link from "next/link";


export default function Modal({isVisible, onClose} : any){
  if(!isVisible) return null;
  return (
    <div className='footer fixed top-0 left-0 flex items-center justify-center w-full h-full bg-black bg-opacity-75'>
      <div className='flex flex-col justify-center h-2/3 mx-auto bg-black p-20 relative bg-opacity-75 w-[45%]'>
        <Link href="#" className="absolute left-[-4%] top-4 text-white Inter-Bold" onClick={() => onClose()}>X</Link>
        <h1 className='text-[#fff] text-3xl py-4 Inter-Bold text-center'>FITNESS TRAINING CENTER</h1>
        <p className='text-gray-600 text-center Inter-Regular mb-5'>
          Welcome to the Fitness Training Center! Our mission is to help you achieve your fitness goals and lead a healthy lifestyle. Whether you're a beginner or an experienced fitness enthusiast, we have programs tailored just for you.
        </p>
        <div className='flex gap-5 mx-auto Inter-Regular flex-wrap'>
            <input className=' border border-solid border-[#808080] text-[#fff] py-[10px] px-[10px] rounded-md' type='text' placeholder='Enter your Name'/>
            <input className=' border border-solid border-[#808080] text-[#fff] py-[10px] px-[10px] rounded-md' type='text' placeholder='Enter your Phone Number'/>
            <input className=' border border-solid border-[#808080] text-[#fff] py-[10px] px-[10px] rounded-md' type='text' placeholder='Weight'/>
            <input className=' border border-solid border-[#808080] text-[#fff] py-[10px] px-[10px] rounded-md' type='text' placeholder='Height'/>
        </div>
        <button className='bg-[#4CAF50] text-white px-6 py-2 mt-4 rounded-md hover:bg-[#45a049] Inter-Bold w-[25%] mx-auto'>
          Book Now
        </button>
      </div>
    </div>
  )
}
