import Image from "next/image";
import Link from "next/link";


const Footer = () => {
  return (
    <div className="pt-[50px] pb-[15px]">
        <div className="wrapper">
            <div className="footer mx-auto py-[100px]">
                <h3 className="text-center text-[40px] text-[#fff] Inter-Bold ">Follow me on <span className="text-[40px] text-[#fff] Inter-Bold hover:underline cursor-pointer"> Instagram</span></h3>
            </div>
            <div className="bottom bg-[#1C2229] py-[60px] mb-[32px]">
                <ul className="w-[90%] mx-auto justify-between flex" >
                    <li className="w-[31%]">
                        <div className="flex items-center w-[40%]">
                          <div className='w-[20%]'>
                            <Image 
                              src={require("../../../public/assets/images/logo.png")} 
                              className="rounded-lg"
                              width={1000}
                              height={1000}
                              alt="image"
                            />
                          </div>
                          <h1 className="text-[25px] text-[#fff] ml-[10px] Lato-Bold">Strengthy</h1>
                        </div>
                        <p className="text-[18px] text-[#BCBCBC] Lato-Regular w-[90%] mb-[40px]">It is a long established fact that a reader will be distracted by the readable.</p>
                        <div className="py-[32px] pl-[36px] border-1 border-solid border-[#485059] bg-[#242A32]">
                          <div className="mb-[40px]">
                            <span className="text-[#A1F65E] Lato-Bold text-[18px]">Call :</span>
                            <h4 className="Lato-Regular text-[18px] text-[#BCBCBC]">01234 987654</h4>
                            <h4 className="Lato-Regular text-[18px] text-[#BCBCBC]">098765 34621</h4>
                          </div>
                          <div className="">
                             <span className="text-[#A1F65E] Lato-Bold text-[18px]">Mail :</span>
                             <h4 className="Lato-Regular text-[18px] text-[#BCBCBC]">contact@fitnessfit.com</h4>
                          </div>
                        </div>
                    </li>
                    <li className="w-[20%]">
                        <Link href="#" className="text-[25px] text-[#fff] Inter-Bold">Utility Pages</Link>
                        <h5 className="text-[18px] text-[#BCBCBC] Lato-Regular mt-[32px]">Style Guide</h5>
                        <h5 className="text-[18px] text-[#BCBCBC] Lato-Regular my-[10px]"> Changelog</h5>
                        <h5 className="text-[18px] text-[#BCBCBC] Lato-Regular my-[10px]"> 404 Page</h5>
                        <h5 className="text-[18px] text-[#BCBCBC] Lato-Regular my-[10px]">Password Protected</h5>
                        <h5 className="text-[18px] text-[#BCBCBC] Lato-Regular my-[10px]">Licenses</h5>
                        <h5 className="text-[18px] text-[#BCBCBC] Lato-Regular ">Contact</h5>
                    </li>
                    <li className="w-[31%]">
                      <Link href="#" className="text-[25px] text-[#fff] Inter-Bold">Newsletter</Link>
                      <div className="border-1 border-solid border-[#BCBCBC] bg-[#242A32] py-[20px] px-[20px] mt-[32px] mb-[30px]">
                          <input type="text" placeholder="enter your email" className="text-[18px] text-[#BCBCBC] Lato-Regular"/>
                      </div>
                      <Link href="#" className=" text-[18px] border-1 border-solid border-[#BABABA] text-[#222] py-[15px] px-[40px] w-[13%] Inter-Medium bg-[#A1F65E]">Subscribe</Link>
                      <div className="my-[40px] flex justify-between w-[30%] items-center">
                        <div className="w-[20%]">
                          <Image 
                              src={require("../../../public/assets/images/instagram 1.svg")} 
                              className=""
                              width={1000}
                              height={1000}
                              alt="image"
                          />
                        </div>
                        <div className="w-[20%]">
                          <Image 
                              src={require("../../../public/assets/images/Fb.last.svg")} 
                              className=""
                              width={1000}
                              height={1000}
                              alt="image"
                          />
                        </div>
                        <div className="w-[20%]">
                          <Image 
                              src={require("../../../public/assets/images/Twiter.last.svg")} 
                              className=""
                              width={1000} 
                              height={1000}
                              alt="image"
                          />
                        </div>
                      </div>
                      <div className="flex justify-between items-center ">
                         <hr className="border-1 border-solid border-[#485059] w-[70%]"></hr>
                         <div className="w-[15%] cursor-pointer">
                          <Image 
                              src={require("../../../public/assets/images/up-arrow.png")} 
                              className=""
                              width={1000}
                              height={1000}
                              alt="image"
                          />
                        </div>
                      </div>
                    </li>
                </ul>
            </div>
            <h2 className=" cursor-pointer text-center Inter-Medium text-[#6A6A6A]">Copy Copyright © <span className="text-[#524FF5] text-[18px] Inter-Medium">FitnessFit</span> | Designed byVictorFlow Templates - Powered by Webflow</h2>
        </div>
    </div>
  )
}

export default Footer
