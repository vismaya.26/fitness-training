"use client"

import Modal from "@/app/auth/_components/Modal";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";


const Header = () => {
   const [showModal, setShowModal] = useState(false);
  return (
    <div className="py-[30px] fixed z-2 bg-white">
       <div className='wrapper flex'>
            <div className="flex items-center w-[40%]">
              <div className='w-[5%]'>
                <Image 
                  src={require("../../../public/assets/images/logo.png")} 
                  className="rounded-lg"
                  width={1000}
                  height={1000}
                  alt="image"
                />
              </div>
              <h1 className="text-[25px] text-[#1D2229] ml-[10px] Lato-Bold">Strengthy</h1>
            </div>
            <ul className="flex flex-row justify-between w-[60%] ">
               <li className="">
                  <Link href="/" className="Lato-Bold text-[18px]">Home</Link>
               </li>
               <li>
                  <Link href="/about" className="Lato-Bold text-[18px]">About</Link>
               </li>
               <li>
                  <Link href="/classess" className="Lato-Bold text-[18px]">Classes</Link>
               </li>
               <li>
                  <Link href="/trainers" className="Lato-Bold text-[18px]">Trainers</Link>
               </li>
               <li>
                  <Link href="/news" className="Lato-Bold text-[18px]">Newss</Link>
               </li>
               <li>
                  <Link href="/contact" className="Lato-Bold text-[18px]">Contact</Link>
               </li>
               <li>
                  <Link href="/" className="py-[19px] px-[51px] bg-[#F2F2F2] Lato-Bold text-[20px]" onClick={() =>setShowModal(true)}>Book Class</Link>
               </li>
            </ul>
      </div>
       <Modal isVisible={showModal} onClose={() => setShowModal(false)} />
    </div>
  )
}

export default Header

